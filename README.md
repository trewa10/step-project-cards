# Step Project "Cards"

# Description
Step project after module Advanced JavaScript.

The task was to implement a web-page where the person could create cards describing planned visits to doctors.
We had to apply ES6 classes, fetch and ES module structure.
Before log in you have to register here https://ajax.test-danit.com/front-pages/cards-register.html

Editing of visit also changes status from done to open. 

## Used technologies
- HTML 5
- CSS 3
- Vanilla JS 
- Bootstrap 5
- Interactjs library (drag & drop)


## Team
Oleh Verbynskyi
Denys Herashchenko
Victor Tyslenko

## Tasks

Team:
- Readme
- Code review and commenting 
- Final general design

Oleh Verbynskyi:
- Functions for working with API
- Classes and methods for Modal windows
- Modal windows design 
- Forms validation

Denys Herashchenko:
- Classes and methods for Cards
- Cards design
- Local storage logic
- Drag'n'Drop

Victor Tyslenko:
- Basic general design
- Serch/filter functions
- Filter form design

